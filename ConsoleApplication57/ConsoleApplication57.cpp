﻿#include <iostream>
#include <ctime>
using namespace std;

typedef unsigned int uint;

uint degree(uint& a, uint& b);

int main()
{
	setlocale(LC_ALL, "ru");

	uint a;
	uint b;

	cout << "Введите число: "; cin >> a; cout << endl;
	cout << "Введите степень: "; cin >> b; cout << endl; cout << endl;

	uint resultDegree = degree(a, b);

	cout << "\tРезультат возведения в степень: " << resultDegree;

	cout << endl; cout << endl;
	return 0;
}

uint degree(uint& a, uint& b) {
	return pow(a, b);
}